import React from "react";
import polygon from '../assets/polygon.svg'
import step1 from '../assets/step1.svg'
import step2 from '../assets/step2.svg'
import step3 from '../assets/step3.svg'
import left from '../assets/left.svg'
import right from '../assets/right.svg'

const Rewards = () => {
  return (
    <div className="rewards">
      <div className="huge">
        <div className="huge-top">
          <h3><img src={polygon} alt={polygon} />Receive Huge Rewards</h3>
          <p>Receive amazing rewards by following these 3 simple steps</p>
        </div>
          <div className="curves">
            <img src={left} alt={left} />
            <img src={right} alt={right} />
          </div>
        <div className="huge-cards">
          <div className="card">
            <img src={step1} alt={step1} />
            <h4>Step 1 - Get your link</h4>
            <p>
              Get your referral code Register and copy referral links or QR
              codes.
            </p>
          </div>
          <div className="card step2">
            <img src={step2} alt={step2} />
            <h4>Step 2 - Send Invitations</h4>
            <p>
            Share your referral code Invite your friends to register through the link or code.
            </p>
          </div>
          <div className="card">
            <img src={step3} alt={step3} />
            <h4>Step 3 - Get Rewards</h4>
            <p>
            Get your referral rewards. Get $8, Give $5 in real-time. Earn unlimited.
            </p>
          </div>
        </div>
      </div>

      <div className="join">
        <p>
          Join and be kept updated and informed. Meanwhile, you can earn huge by
          joining, sharing and experiencing some amazing rewards for simply
          joining.
        </p>
        <button>Join the Waitlist Now</button>
      </div>
      <button>Visit Ultrainfinity Exchange</button>
    </div>
  );
};

export default Rewards;
