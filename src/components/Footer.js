import React from "react";
import system from "../assets/system.svg";
import ios from "../assets/ios.svg";
import andriod from "../assets/andriod.svg";
import facebook from "../assets/socials/facebook.svg";
import instagram from "../assets/socials/instagram.svg";
import youtube from "../assets/socials/youtube.svg";
import twitter from "../assets/socials/twitter.svg";
import pininterest from "../assets/socials/pininterest.svg";
import linkedin from "../assets/socials/linkedin.svg";
import discord from "../assets/socials/discord.svg";
import tiktok from "../assets/socials/tiktok.svg";
import vk from "../assets/socials/vk.svg";
import reddit from "../assets/socials/reddit.svg";
import telegram from "../assets/socials/telegram.svg";
import whatsapp from "../assets/socials/whatsapp.svg";
import messenger from "../assets/socials/messenger.svg";
import viber from "../assets/socials/viber.svg";
import signal from "../assets/socials/signal.svg";
import snapchat from "../assets/socials/snapchat.svg";
import likee from "../assets/socials/likee.svg";
import line from "../assets/socials/line.svg";
import quora from "../assets/socials/quora.svg";
import medium from "../assets/socials/medium.svg";

const Footer = () => {
  return (
    <footer>
      <div className="footer-top">
        <div className="footer-text">
          <h3>Try the latest version of our app</h3>
          <p>We are giving you $10 for downloading and using our app</p>
          <div className="footer-btn">
            <button>
              <img src={ios} alt={ios} />
              Download our iOS App
            </button>
            <button>
              <img src={andriod} alt={andriod} />
              Download our Android App
            </button>
          </div>
        </div>
        <img src={system} alt={system} />
      </div>
      <div className="footer-bottom">
        <div className="footer-socials">
        <img src={facebook} alt="" />
        <img src={instagram} alt="" />
        <img src={youtube} alt="" />
        <img src={twitter} alt="" />
        <img src={pininterest} alt="" />
        <img src={linkedin} alt="" />
        <img src={discord} alt="" />
        <img src={tiktok} alt="" />
        <img src={vk} alt="" />
        <img src={reddit} alt="" />
        <img id="pp" src={telegram} alt="" />
        <img id="pp" src={whatsapp} alt="" />
        <img id="pp" src={messenger} alt="" />
        <img id="pp" src={viber} alt="" />
        <img id="pp" src={signal} alt="" />
        <img id="pp" src={snapchat} alt="" />
        <img id="pp" src={likee} alt="" />
        <img id="pp" src={line} alt="" />
        <img id="tp" src={quora} alt="" />
        <img id="tp" src={medium} alt="" />
        </div>
        <p className="footer-bottom-t">
          We look forward to Welcoming and Rewarding you.
        </p>
      </div>
    </footer>
  );
};

export default Footer;
