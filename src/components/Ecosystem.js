import React from "react";
import polygon from '../assets/polygon.svg'
import center from '../assets/center.svg'

const Ecosystem = () => {
  return (
    <div className="ecosystem">
      <span>OUR UNIQUE PRODUCTS</span>
      <h3><img src={polygon} alt={polygon} />The Ultainfinity Ecosystem</h3>
      <p>
        The blockchain network and each of the cryptocurrencies and
        decentralised finance platforms are part of a growing global ecosystem
        that will facilitate wealth creation, transfer, distribution and
        management.
      </p>
      <div className="eco">
        <img src={center} alt={center} />
        <div className="eco-p">
          <p>Ultainfinity Blockchain</p>
          <p>Ultainfinity Exchange</p>
          <p>Ultainfinity Wallet</p>
          <p>Ultainfinity Wealth</p>
        </div> 
      </div>
    </div>
  );
};

export default Ecosystem;
