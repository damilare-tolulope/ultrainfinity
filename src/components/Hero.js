import React from "react";
import logo from '../assets/logo.svg'
import telegram from '../assets/telegram.svg'
import whatsapp from '../assets/whatsapp.svg'
import viber from '../assets/viber.svg'
import signal from '../assets/signal.svg'
import discord from '../assets/discord.svg'
import twitter from '../assets/twitter.svg'
import line from '../assets/line.svg'
import uk from '../assets/uk.svg'
import arrow from '../assets/arrow.svg'

const Hero = () => {
  return (
    <div className="hero">
      <div className="logo">
        <img src={logo} alt={logo} />
      </div>
      <div className="hero-text">
        <h2>We Are Launching Soon</h2>
        <p>
          Ultainfinity Exchange, the beginning of a new stable and supercharged
          wealth era in cryptocurrency, never seen before.
        </p>
      </div>
      <button>Join our Waiting List</button>

      <div className="waiting-list">
        <h4>Join our Waiting List</h4>
        <p>We have over 1 million in our various global waiting list</p>
        <div className="input">
            <select>
              <option><img src={uk} alt={uk}/></option>
            </select>
            <input type="text" placeholder="Enter phone number" />
        </div>
        <button>Send <img src={arrow} alt={arrow} /></button>
        <div className="socials">
            <img src={telegram} alt={telegram} />
            <img src={whatsapp} alt={whatsapp} />
            <img src={viber} alt={viber} />
            <img src={signal} alt={signal} />
            <img src={discord} alt={discord} />
            <img src={line} alt={line} />
            <img src={twitter} alt={twitter} />
        </div>
      </div>
    </div>
  );
};

export default Hero;
