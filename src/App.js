import React from 'react'
import './App.css'
import Hero from './components/Hero'
import Ecosystem from './components/Ecosystem.js'
import Rewards from './components/Rewards'
import Footer from './components/Footer'

const App = () => {
  return (
    <div className='App'>
      <Hero />
      <Ecosystem />
      <Rewards />
      <Footer />  
    </div>
  )
}

export default App